import re


#filename = 'kepco-BOP502M.db'
filename = 'kepco.db'

match = ['HIHI','LOLO','HIGH','LOW','HHSV','LLSV','HSV','LSV','HYST']
#pattern = re.compile(r"record\w*pe$", re.IGNORECASE)

fields = []
record = []
all_records = []

with open(filename, 'rt') as myfile:
    for line in myfile:
       line = line.partition('#')[0] # remove comments 

       if re.match(r'^\s*$', line):  # remove empty line
          continue

       line = re.sub(' ','',line)    # remove space

       if re.search('record',line):  # search for records
          name = re.split(',',line)
          named = re.sub('["{]','',name[1].strip('\n'))[:-1]  # remove chars "{ 
          record.append(named)
          continue

       if re.search('DESC',line):  # search for DESC field
          desc = re.split(',',line)
          desc = re.sub('["{]','',desc[1].strip('\n'))[:-1]  # remove charas "{ 
          record.append(desc)
          continue

       if any(word in line for word in match): # find all match words
          fieldsplit = re.split("field",line) 
          fieldsplit = list(filter(None,fieldsplit))   # remove empty string at the beginning
          for f in fieldsplit:
             field = re.sub('["()]','',f).strip('\n')  # remove chars "() 
             fields.append(re.split(',',field))

       if re.search('}',line):
          record.append(fields)
          fields = []
          all_records.append(record)
          record = []

#print(all_records)
#print('nr of records = ', len(all_records))

#############################################################################

from lxml import etree

##############################################
#
#def addComponent(parent, name):
#    child = etree.SubElement(parent, 'component', name=name)
#    return child
#
##############################################
#
#def addGuidance(parent, title, details):
#    child = etree.SubElement(parent, 'component', name=name)
#    return child
#
#guidance = etree.SubElement(component, 'guidance')
#title = etree.SubElement(guidance, 'title')
#title.text='General chopper Guidance'
#
##############################################

def addElement(parent, tag, name='', value = ''):
    if not name:
       child = etree.SubElement(parent, tag)
    elif not value:
       child = etree.SubElement(parent, tag, name=name)

    if value:
        child.text = value

    return child

##############################################


root = etree.Element('config', name='Sample Environment')

for record in all_records:
    component = addElement(root,    'component', 'Kepco')
    guidance  = addElement(component,'guidance')
    title     = addElement(guidance, 'title',       value='????')
    details   = addElement(guidance, 'details',     value='????')
    pv        = addElement(component,'pv',          name=record[0])
    descript  = addElement(pv,       'description', value=record[1])
    for field in record[2]:
       pv        = addElement(component,field[0],          name=field[1])
       enabled   = addElement(pv,       'enabled',     value='true or false')
       latching  = addElement(pv,       'latching',    value='true or false')
       annunciat = addElement(pv,       'annunciating',value='????')
    

print(etree.tostring(root, pretty_print=True, encoding='unicode'))


#et = etree.ElementTree(config)
#with open('sample.xml', 'wb') as f:
#    et.write(f, encoding="utf-8", xml_declaration=True, pretty_print=True)
