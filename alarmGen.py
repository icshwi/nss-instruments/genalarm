from lxml import etree

config = etree.Element('config', name='test_techgroup')

component = etree.SubElement(config, 'component', name='choppers')

guidance = etree.SubElement(component, 'guidance')
title = etree.SubElement(guidance, 'title')
title.text='General chopper Guidance'
details = etree.SubElement(guidance, 'details')
details.text='This not happen, it cannot happen, yet it happens'   


pv = etree.SubElement(component, 'pv', name='Istr1:Chopper1:speed')
description = etree.SubElement(pv, 'description')
description.text='Limit has been exceeded'
enabled = etree.SubElement(pv, 'enabled')
enabled.text='true'
latching = etree.SubElement(pv, 'latching')
latching.text='true'
annunciating = etree.SubElement(pv,'annunciating')
annunciating.text='false'



print(etree.tostring(config, pretty_print=True))

et = etree.ElementTree(config)
with open('sample.xml', 'wb') as f:
    et.write(f, encoding="utf-8", xml_declaration=True, pretty_print=True)

