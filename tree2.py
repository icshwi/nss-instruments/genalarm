import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QTreeView, QInputDialog, QLineEdit, QMenu
from PyQt5.Qt import QStandardItemModel, QStandardItem, QHeaderView
from PyQt5.QtGui import QFont, QColor
from PyQt5.QtCore import Qt, QModelIndex


class StandardItem(QStandardItem):
    def __init__(self, txt='', font_size=12, set_bold=False, color=QColor(0, 0, 0)):
        super().__init__()

        fnt = QFont('Open Sans', font_size)
        fnt.setBold(set_bold)

        self.setEditable(True)
        self.setForeground(color)
        self.setFont(fnt)
        self.setText(txt)


class AppDemo(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('World Country Diagram')
        self.resize(700, 400)
        # create an instance of QTreeViewCustom
        myTreeView = QTreeViewCustom()

        self.setCentralWidget(myTreeView)

class QTreeViewCustom(QTreeView):
    def __init__(self):
        super().__init__()

        self.setHeaderHidden(False)
       # self.setHorizontalHeaderLabels(['aaa', 'bbb', 'ccc', 'ddd'])
        self.treeModel = QStandardItemModel()
        self.rootNode = self.treeModel.invisibleRootItem()
        labels = ['Components','Name','Title','Details','Description','Enabled','Latching','annunciating']
        self.treeModel.setHorizontalHeaderLabels(labels)

        # we create the first component
        alarm = StandardItem('Alarm', 16, set_bold=True)
        #self.drawBranches(QPainter())
        self.rootNode.appendRow(alarm)
        self.setModel(self.treeModel)
        self.expandAll()

        # 1 click
        self.clicked.connect(self.getValue)
        # 2 click
        #self.doubleClicked.connect(self.getValue)

        # ---------------------------------------------------------------------
        # configurint the context menu
        # ---------------------------------------------------------------------
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.openMenu)


    def getValue(self, value):
        print("value: %s, data:%s, row:%s, col:%s" % (value, value.data(), value.row(), value.column()))

    def getNewItem(self):
        text, okPressed = QInputDialog.getText(self, "Adding new item...","Entry item:", QLineEdit.Normal, "")
        if okPressed and text != '':
            return text
        else:
            return None

    def openMenu(self, position):
    
        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            level = 0
            index = indexes[0]
            while index.parent().isValid():
                index = index.parent()
                level += 1
        # get the clicked item based on position
        itemIndex = self.indexAt(position)

        menu = QMenu()
        add_component = menu.addAction(self.tr("Add component"))
        add_component.triggered.connect(lambda:self.addComponent(itemIndex))
        # edit
        edit_component = menu.addAction(self.tr("Edit component"))
        edit_component.triggered.connect(lambda :self.editComponent(itemIndex))
        # del
        del_component = menu.addAction(self.tr("Delete component"))
        del_component.triggered.connect(lambda:self.deleteComponent(itemIndex))
        # add db
        add_db = menu.addAction(self.tr("Add EPICS db"))
        add_db.triggered.connect(lambda:self.addDb(itemIndex))

        menu.exec_(self.viewport().mapToGlobal(position))

    # Add item
    def addComponent(self, item):
        print("item: %s, data:%s, row:%s, col:%s" % (item, item.data(), item.row(), item.column()))
        newItem = self.getNewItem()
        if newItem:
            newTreeItem = StandardItem(newItem, 14)
            self.treeModel.itemFromIndex(item).appendRow(newTreeItem)
            self.expandAll()
    # Edit item
    def editComponent(self, item):
        print("item: %s, data:%s, row:%s, col:%s" % (item, item.data(), item.row(), item.column()))

    # Add EPICS Db
    def addDb(self, item):
        print("item: %s, data:%s, row:%s, col:%s" % (item, item.data(), item.row(), item.column()))


    # Delete item
    def deleteComponent(self, item):
        print("item: %s, data:%s, row:%s, col:%s" % (item, item.data(), item.row(), item.column()))
        itemNode = self.treeModel.itemFromIndex(item)
        if itemNode.parent():
            itemNode.parent().removeRow(itemNode.row())
        else:
            itemNode.removeRow(itemNode.row())

app = QApplication(sys.argv)
 
demo = AppDemo()
demo.show()
 
sys.exit(app.exec_())
